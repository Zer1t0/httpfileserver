#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use]
extern crate rocket;
use rocket::config::{Config, LoggingLevel};
use rocket::response::NamedFile;
use rocket::Data;
use rocket::State;
use std::io;
use std::path::{Path, PathBuf};

use clap::{App, Arg};

fn args() -> App<'static, 'static> {
    App::new(env!("CARGO_PKG_NAME"))
        .about(env!("CARGO_PKG_DESCRIPTION"))
        .version(env!("CARGO_PKG_VERSION"))
        .arg(
            Arg::with_name("port")
                .long("port")
                .short("p")
                .takes_value(true)
                .default_value("8000")
                .validator(is_u16)
                .help("Port to listen"),
        )
        .arg(
            Arg::with_name("bind")
                .long("bind")
                .short("b")
                .takes_value(true)
                .default_value("0.0.0.0")
                .validator(is_address)
                .help("Address to listen"),
        )
        .arg(
            Arg::with_name("directory")
                .long("directory")
                .short("d")
                .takes_value(true)
                .default_value(".")
                .validator(is_path)
                .help("Directory to serve files"),
        )
        .arg(
            Arg::with_name("log-level")
                .long("log-level")
                .takes_value(true)
                .possible_values(&["off", "normal", "debug", "critical"])
                .help("Verbosity of logs"),
        )
        .arg(
            Arg::with_name("no-upload")
                .long("no-upload")
                .help("Disable files upload"),
        )
        .arg(
            Arg::with_name("no-download")
                .long("no-download")
                .help("Disable files download"),
        )
}

pub fn is_u16(v: String) -> Result<(), String> {
    v.parse::<u16>().map_err(|_| {
        format!(
            "Incorrect value '{}' must be an unsigned integer of 16 bits (u16)",
            v
        )
    })?;

    return Ok(());
}

pub fn is_address(v: String) -> Result<(), String> {
    let mut config = Config::active().unwrap();
    config.set_address(&v).map_err(|_| {
        format!("Incorrect value '{}' must be an IP address or hostname", v)
    })?;

    return Ok(());
}

pub fn is_path(v: String) -> Result<(), String> {
    if !Path::new(&v).exists() {
        return Err(format!("Invalid path `{}`", v));
    }

    return Ok(());
}

struct Arguments {
    pub port: u16,
    pub bind: String,
    pub directory: String,
    pub log_level: Option<LoggingLevel>,
    pub upload: bool,
    pub download: bool,
}

impl Arguments {
    fn parse_args() -> Self {
        let matches = args().get_matches();
        let port = matches
            .value_of("port")
            .unwrap()
            .parse()
            .expect("Error parsing port");
        let bind = matches
            .value_of("bind")
            .unwrap()
            .parse()
            .expect("Error parsing bind address");
        let directory = matches
            .value_of("directory")
            .unwrap()
            .parse()
            .expect("Error parsing directory");

        let log = matches
            .value_of("log-level")
            .map(|s| s.to_string().parse().expect("Error parsing log level"));

        let upload = !matches.is_present("no-upload");
        let download = !matches.is_present("no-download");

        Self {
            port: port,
            bind: bind,
            directory: directory,
            log_level: log,
            upload,
            download,
        }
    }
}

fn main() {
    let args = Arguments::parse_args();

    let mut config = Config::active().unwrap();
    config.set_address(args.bind).unwrap();
    config.set_port(args.port);

    if let Some(log_level) = args.log_level {
        config.set_log_level(log_level);
    }

    let mut routes = Vec::new();

    if args.download {
        routes.append(&mut routes![index]);
    }

    if args.upload {
        routes.append(&mut routes![index_post]);
    }

    rocket::custom(config)
        .mount("/", routes)
        .manage(args.directory)
        .launch();
}

#[get("/<filepath..>")]
fn index(filepath: PathBuf, base_dir: State<String>) -> Option<NamedFile> {
    let path = Path::new(&base_dir.inner()).join(filepath);
    NamedFile::open(path).ok()
}

#[post("/<filepath..>", format = "binary", data = "<input>")]
fn index_post(
    filepath: PathBuf,
    input: Data,
    base_dir: State<String>,
) -> io::Result<String> {
    let path = Path::new(&base_dir.inner()).join(filepath);
    input
        .stream_to_file(path)
        .map(|n| format!("Wrote {} bytes", n))
}
