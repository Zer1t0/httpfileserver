# httpfileserver

A simple HTTP server to download and upload files.

## Installation

```shell
git clone https://gitlab.com/Zer1t0/httpfileserver.git
cd httpfileserver/
rustup override set nightly
cargo build --release
```

To build static in Windows set the environment variable:
```
RUSTFLAGS="-C target-feature=+crt-static"
```
For example, in powershell use:
```
$env:RUSTFLAGS='-C target-feature=+crt-static'; cargo build --release
```

## Examples

### Download file
```shell
curl 127.0.0.1:8000/test
```

```powershell
Invoke-WebRequest http://127.0.0.1/sample.bin -outfile sample.bin
```

### Upload file
```shell
curl 127.0.0.1:8000/test_up -H "Content-Type: application/octet-stream" --data-binary "@/etc/hosts"
```

```powershell
Invoke-WebRequest -contenttype "application/octet-stream" -method POST -infile sample.bin  http://127.0.0.1/sample.bin
```


## Tips

### Disable colors
Set the environment variable `ROCKET_CLI_COLORS=off`.
